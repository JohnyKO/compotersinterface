﻿using System;

namespace Computers.Model.IModel
{
    interface IROM : IDevice
    {
        int CountOfGB { get; }
        void Clear();
    }
}
