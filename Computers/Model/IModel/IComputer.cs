﻿using System;

namespace Computers.Model.IModel
{
    abstract class Computer : IDevice
    {
        public IProcessor processor { get; set; }
        public IROM rom { get; set; }
        public IVideCard videoCard { get; set; }

        protected Computer()
        {
            this.processor = null;
            this.rom = null;
            this.videoCard = null;
        }

        protected Computer(IProcessor processor, IROM rom, IVideCard videoCard)
        {
            this.processor = processor;
            this.rom = rom;
            this.videoCard = videoCard;
        }

        virtual public void Restart()
        {
            this.processor.Restart();
            this.rom.Restart();
            this.videoCard.Restart();
        }

        virtual public void Start()
        {
            this.processor.Start();
            this.rom.Start();
            this.videoCard.Start();
        }

        virtual public void Stop()
        {
            this.processor.Stop();
            this.rom.Stop();
            this.videoCard.Stop();
        }

        public bool IsValide()
        {
            if ((processor == null) || (rom == null) || (videoCard == null))
                return false;
            else return true;
        }

        public override string ToString()
        {
            return $"{this.processor.ToString()}" +
                $"\n {this.rom.ToString()}" +
                $"\n{this.videoCard.ToString()}";
        }
    }
}
