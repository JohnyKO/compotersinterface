﻿using System;
using Computers.Model.Enums;

namespace Computers.Model.IModel
{
    interface IVideCard : IDevice
    {
        int CountOfMB { get; }
        VideoCardType videoCardType { get; }
    }
}
