﻿using System;

namespace Computers.Model.IModel
{
    interface IProcessor : IDevice
    {
        float CPU { get; }
        int CountOfCores { get; }
    }
}
