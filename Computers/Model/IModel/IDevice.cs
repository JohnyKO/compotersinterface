﻿using System;

namespace Computers.Model.IModel
{
    interface IDevice
    {
        void Start();
        void Restart();
        void Stop();
    }
}
