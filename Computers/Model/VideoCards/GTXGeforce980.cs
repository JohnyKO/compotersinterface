﻿using System;
using Computers.Model.Enums;
using Computers.Model.IModel;

namespace Computers.Model.VideoCards
{
    class GTXGeforce980 : IVideCard
    {
        public int CountOfMB { get; private set; }
        public VideoCardType videoCardType { get; private set; }

        public GTXGeforce980()
        {
        }

        public GTXGeforce980(int CountOfMB, VideoCardType videoCardType)
        {
            this.CountOfMB = CountOfMB;
            this.videoCardType = videoCardType;
        }

        public void Restart()
        {
            Console.WriteLine("GTXGeforce980:Restart() called");
        }

        public void Start()
        {
            Console.WriteLine("GTXGeforce980:Start() called");
        }

        public void Stop()
        {
            Console.WriteLine("GTXGeforce980:Stop() called");
        }

        public override string ToString()
        {
            return $"GTXGeforce980 count of MB = {CountOfMB}, video card type = {videoCardType}";
        }
    }
}
