﻿using System;
using Computers.Model.Enums;
using Computers.Model.IModel;

namespace Computers.Model.VideoCards
{
    class RadeonGraphicsHD7770 : IVideCard
    {
        public int CountOfMB { get; private set; }
        public VideoCardType videoCardType { get; private set; }

        public RadeonGraphicsHD7770()
        {
        }

        public RadeonGraphicsHD7770(int CountOfMB, VideoCardType videoCardType)
        {
            this.CountOfMB = CountOfMB;
            this.videoCardType = videoCardType;
        }

        public void Restart()
        {
            Console.WriteLine("RadeonGraphicsHD7770:Restart() called");
        }

        public void Start()
        {
            Console.WriteLine("RadeonGraphicsHD7770:Start() called");
        }

        public void Stop()
        {
            Console.WriteLine("RadeonGraphicsHD7770:Stop() called");
        }

        public override string ToString()
        {
            return $"RadeonGraphicsHD7770 count of MB = {CountOfMB}, video card type = {videoCardType}";
        }
    }
}
