﻿using System;

namespace Computers.Model.Enums
{
    enum VideoCardType
    {
        DDR3,
        DDR4,
        DDR5
    };
}
