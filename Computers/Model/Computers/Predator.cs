﻿using System;
using Computers.Model.IModel;

namespace Computers.Model.Computers
{
    class Predator : Computer
    {
        public Predator() : base() {}

        public Predator(IProcessor processor, IROM rom, IVideCard videoCard)
            : base(processor, rom, videoCard) {}
    }
}
