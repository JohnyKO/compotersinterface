﻿using System;

using Computers.Model.IModel;

namespace Computers.Model.Computers
{
    class InvisionLab : Computer
    {
        public InvisionLab() : base() { }
        public InvisionLab(IProcessor processor, IROM rom, IVideCard videoCard)
            : base(processor, rom, videoCard) { }
    }
}
