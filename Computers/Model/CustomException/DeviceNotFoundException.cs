﻿using System;

namespace Computers.Model.CustomException
{
    class DeviceNotFoundException : Exception
    {
        public DeviceNotFoundException() : base("Device Not Found!!!") {}
    }
}
