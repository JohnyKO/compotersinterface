﻿using System;
using Computers.Model.IModel;

namespace Computers.Model.ROMs
{
    class HyperXROM : IROM
    {
        public int CountOfGB { get; private set; }

        public HyperXROM()
        {
        }

        public HyperXROM(int countOfGB)
        {
            CountOfGB = countOfGB;
        }

        public void Clear()
        {
            Console.WriteLine("HyperXROM:Clear() called");
        }

        public void Restart()
        {
            Console.WriteLine("HyperXROM:Restart() called");
        }

        public void Start()
        {
            Console.WriteLine("HyperXROM:Start() called");
        }

        public void Stop()
        {
            Console.WriteLine("HyperXROM:Stop() called");
        }

        public override string ToString()
        {
            return $"HyperXROM count of GB = {CountOfGB}";
        }
    }
}
