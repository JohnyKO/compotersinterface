﻿using System;
using Computers.Model.IModel;

namespace Computers.Model.ROMs
{
    class DellROM : IROM
    {
        public int CountOfGB { get; private set; }

        public DellROM()
        {
        }

        public DellROM(int countOfGB)
        {
            CountOfGB = countOfGB;
        }

        public void Clear()
        {
            Console.WriteLine("DellROM:Clear() called");
        }

        public void Restart()
        {
            Console.WriteLine("DellROM:Restart() called");
        }

        public void Start()
        {
            Console.WriteLine("DellROM:Start() called");
        }

        public void Stop()
        {
            Console.WriteLine("DellROM:Stop() called");
        }

        public override string ToString()
        {
            return $"DellROM count of GB = {CountOfGB}";
        }
    }
}
