﻿using System;
using Computers.Model.IModel;

namespace Computers.Model.Processors
{
    class IntelCoreI5 : IProcessor
    {
        //Properties of classes
        public float CPU { get; private set; }
        public int CountOfCores { get; private set; }

        public IntelCoreI5()
        {
        }

        public IntelCoreI5(float cPU, int countOfCores)
        {
            CPU = cPU;
            CountOfCores = countOfCores;
        }

        public void Restart()
        {
            Console.WriteLine("IntelCoreI5:Restart() called");
        }

        public void Start()
        {
            Console.WriteLine("IntelCoreI5:Start() called");
        }

        public void Stop()
        {
            Console.WriteLine("IntelCoreI5:Stop() called");
        }

        public override string ToString()
        {
            return $"IntelCoreI5 CPU = {CPU}, count of cores = {CountOfCores}";
        }
    }
}
