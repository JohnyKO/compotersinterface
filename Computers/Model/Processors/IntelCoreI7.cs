﻿using System;
using Computers.Model.IModel;

namespace Computers.Model.Processors
{
    class IntelCoreI7 : IProcessor
    {
        public float CPU { get; private set; }
        public int CountOfCores { get; private set; }

        public IntelCoreI7()
        {
        }

        public IntelCoreI7(float cPU, int countOfCores)
        {
            CPU = cPU;
            CountOfCores = countOfCores;
        }

        public void Restart()
        {
            Console.WriteLine("IntelCoreI7:Restart() called");
        }

        public void Start()
        {
            Console.WriteLine("IntelCoreI7:Start() called");
        }

        public void Stop()
        {
            Console.WriteLine("IntelCoreI7:Stop() called");
        }

        public override string ToString()
        {
            return $"IntelCoreI7 CPU = {CPU}, count of cores = {CountOfCores}";
        }
    }
}
