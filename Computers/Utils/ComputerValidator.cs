﻿using System;
using Computers.Model.IModel;
using Computers.Model.CustomException;

namespace Computers.Utils
{
    class ComputerValidator
    {
        public ComputerValidator(Computer computer)
        {
            if (!computer.IsValide())
                throw new DeviceNotFoundException();
        }
    }
}
