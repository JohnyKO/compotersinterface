﻿using System;

using Computers.Model.Processors;
using Computers.Model.Computers;
using Computers.Model.ROMs;
using Computers.Model.VideoCards;
using Computers.Model.Enums;
using Computers.Model.CustomException;
using Computers.Utils;

namespace Computers.Source
{
    class MainClass
    {
        public static void Main()
        {
            Predator predator1 = new Predator(new IntelCoreI5(2.7f, 4),
                                              new DellROM(8),
                                              new RadeonGraphicsHD7770(2048, VideoCardType.DDR3));

            InvisionLab invisionLab = new InvisionLab();
            try
            {
                new ComputerValidator(invisionLab);
                invisionLab.Start();
                invisionLab.Stop();


            }
            catch(DeviceNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            invisionLab.processor = new IntelCoreI7(2.2f, 6);
            invisionLab.rom = new HyperXROM(6);

            Console.WriteLine("Predator1 start");
            predator1.Start();


            Console.WriteLine("\nPredator1 stop");
            predator1.Stop();

            Console.WriteLine(invisionLab.IsValide().ToString());


            Console.ReadLine();
        }
    }
}
